﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using RingCentral;
using RingCentral.Http;
using Newtonsoft.Json;

namespace CallManager.Prototype
{
	public partial class _Default : Page
	{

		#region Properties

		public string QueryCode
		{
			get
			{
				return Request.QueryString.Get("code");
			}
		}
		public string QueryState
		{
			get
			{
				return Request.QueryString.Get("state");
			}
		}

		#endregion

		#region Global
		protected SDK ringsdk;
		#endregion
		
		#region Page Functions

		protected void Page_Init(Object sender, EventArgs e)
		{

			string RingCentralAPI_URL = ConfigurationManager.AppSettings["RingCentralAPI_URL"];
			string RingCentralAPI_Key = ConfigurationManager.AppSettings["RingCentralAPI_Key"];
			string RingCentralAPI_Secret = ConfigurationManager.AppSettings["RingCentralAPI_Secret"];
			string RingCentralAPI_AppName = ConfigurationManager.AppSettings["RingCentralAPI_AppName"];
			ringsdk = new SDK(RingCentralAPI_Key, RingCentralAPI_Secret, RingCentralAPI_URL, RingCentralAPI_AppName);

			if (!IsPostBack && QueryState == "AuthRequest" && QueryCode != null && QueryCode != String.Empty)
			{
				//This is the pass back from Ring Central Authentication  - we need to take the code from the queryline and use it to request an Access Token
				RequestAuthTokenFromRingCentral();
			}
			else
			{
				//Request Refresh of AccessToken or re authenticate user
				if (!ringsdk.Platform.LoggedIn())
				{
					
					if (Session["RingAuthToken"] != null)
					{
						//Set Auth Data into SDK and refresh Access Token
						ringsdk.Platform.Auth.SetData((Dictionary<string, string>)Session["RingAuthToken"]);
						ringsdk.Platform.Refresh();
					}
					else
					{
						//We have no auth data so Request login (redirectects user to ring central login page)
						//after login - user will be redirected back to here with QueryState "AuthRequest"
						RequestAuthCodeFromRingCentral();
					}
				}
				else
				{
					//ringsdk.Platform.Refresh(); //done by Loggedin Check
				}
			}
		}

		protected void Page_Load(object sender, EventArgs e)
		{
			
		}
		
		protected void btnCurrentUserInfo_Click(object sender, EventArgs e)
		{
			UpdateCurrentUserInformation();
		}

		protected void btnGetCallLog_Click(object sender, EventArgs e)
		{
			GetCurrentUsersCallLog();
		}
		
		protected void btnGetGroupCallLog_Click(object sender, EventArgs e)
		{
			GetGroupCallLog("123");
		}

		protected void btnGetCallMessages_Click(object sender, EventArgs e)
		{
			GetCurrentUsersMessages();
		}

		protected void btnGetGroupMessages_Click(object sender, EventArgs e)
		{
			GetGroupMessages("123");
		}
		
		protected void btnGetGroupList_Click(object sender, EventArgs e)
		{
			GetGroupList();
		}

		#endregion

		#region Helpers

		/// <summary>
		/// Redirect to Ring Central to Authenticate User
		/// Ring Central will return user to url of our choosing
		/// </summary>
		private void RequestAuthCodeFromRingCentral()
		{
			string RingCentralAPI_AuthCallBack = ConfigurationManager.AppSettings["RingCentralAPI_AuthCallBack"];

			string WebRequest_URL = ringsdk.Platform.AuthorizeUri(RingCentralAPI_AuthCallBack, "AuthRequest");
			
			Response.Redirect(WebRequest_URL);

		}

		/// <summary>
		/// using the code provided by the Ring Central login - we can request a token for all further communication on behalf of this user
		/// </summary>
		private void RequestAuthTokenFromRingCentral()
		{

			string RingCentralAPI_AuthCallBack = ConfigurationManager.AppSettings["RingCentralAPI_AuthCallBack"];
			
			var response = ringsdk.Platform.Authenticate(QueryCode, RingCentralAPI_AuthCallBack);// "https://requestb.in/12mbm901");
			
			var AuthData = ringsdk.Platform.Auth.GetData();
			Session["RingAuthToken"] = AuthData;

		}

		/// <summary>
		/// Get current logged in user information and update the Call Managers database
		/// </summary>
		private void UpdateCurrentUserInformation()
		{
			string extensionID = ringsdk.Platform.Auth.GetData()["owner_id"];

			var request1 = new Request(String.Format("/restapi/v1.0/account/~/extension/{0}/", extensionID));
			var response1 = ringsdk.Platform.Get(request1);
			if (response1.OK)
			{
				txtResponse.Text = response1.Body.ToString();

				using (var cmEntities = new DataLayer.CMEntities())
				{
					var userRecord = from u in cmEntities.data_Users where u.RingCentralExtensionID == extensionID select u;
					DataLayer.data_Users editUser;

					if (userRecord.Count() == 0)
					{
						editUser = cmEntities.data_Users.Create();
						editUser.RingCentralExtensionID = extensionID;
						editUser.ID = Guid.NewGuid();
						cmEntities.data_Users.Add(editUser);
					}
					else
					{
						editUser = userRecord.First();
					}

					editUser.Name = response1.Json["name"].ToString();
					cmEntities.SaveChanges();

				}
			}
			else
			{
				txtResponse.Text = "ERROR: " + response1.Body.ToString();
			}
		}
		
		private void GetGroupList()
		{
			//TODO: Sort AccountID out

			//  "/restapi/v1.0/account/11111111/extension?type=Department";
			string extensionID = ringsdk.Platform.Auth.GetData()["owner_id"];

			var request1 = new Request(String.Format("/restapi/v1.0/account/168732004/extension?type=Department", extensionID));
			var response1 = ringsdk.Platform.Get(request1);
			if (response1.OK)
			{
				txtResponse.Text = response1.Body.ToString();
			}
			else
			{
				txtResponse.Text = "ERROR: " + response1.Body.ToString();
			}

		}

		private void GetCurrentUsersCallLog()
		{
			// restapi / v1.0 / account /{ accountId}/ extension /{ extensionId}/ call - log

			string extensionID = ringsdk.Platform.Auth.GetData()["owner_id"];

			var request1 = new Request(String.Format("/restapi/v1.0/account/~/extension/{0}/call-log", extensionID));
			var response1 = ringsdk.Platform.Get(request1);
			if (response1.OK)
			{
				txtResponse.Text = response1.Body.ToString();
			}
			else
			{
				txtResponse.Text = "ERROR: " + response1.Body.ToString();
			}
		}

		private void GetGroupCallLog(string GroupID)
		{
			//TODO: This will (if it works) grab all calls - we want a sub set
			
			var request1 = new Request("/restapi/v1.0/account/168732004/extension/~/call-log");
			var response1 = ringsdk.Platform.Get(request1);
			if (response1.OK)
			{
				txtResponse.Text = response1.Body.ToString();
			}
			else
			{
				txtResponse.Text = "ERROR: " + response1.Body.ToString();
			}
		}

		private void GetCurrentUsersMessages()
		{
			///restapi/v1.0/account/{accountId}/extension/{extensionId}/message-store
			///
			string extensionID = ringsdk.Platform.Auth.GetData()["owner_id"];

			var request1 = new Request(String.Format("restapi/v1.0/account/~/extension/{0}/message-store?dateFrom=2017-01-01", extensionID));
			var response1 = ringsdk.Platform.Get(request1);
			if (response1.OK)
			{
				txtResponse.Text = response1.Body.ToString();
			}
			else
			{
				txtResponse.Text = "ERROR: " + response1.Body.ToString();
			}


		}

		private void GetGroupMessages(string v)
		{
			//All Messages - Needs filtering by me

			var request1 = new Request("/restapi/v1.0/account/168732004/extension/~/message-store");
			var response1 = ringsdk.Platform.Get(request1);
			if (response1.OK)
			{
				txtResponse.Text = response1.Body.ToString();
			}
			else
			{
				txtResponse.Text = "ERROR: " + response1.Body.ToString();
			}



		}

		/// <summary>
		/// Just an example request for reference
		/// </summary>
		/// 
		private void examplerequest()
		{

			var requestBody = new
			{
				firstName = "my",
				lastName = "mobile",
				homePhone = "+447977484024"
			};
			var request1 = new Request("/restapi/v1.0/account/~/extension/~/address-book/contact", JsonConvert.SerializeObject(requestBody));
			var response1 = ringsdk.Platform.Post(request1);
			if (response1.OK)
			{
				//response1.Json["id"].ToString();

				txtResponse.Text = response1.Response.ToString();
			}
			else
			{
				txtResponse.Text = "ERROR: " + response1.Response.ToString();
			}

		}

		#endregion

		protected void btnManageGroups_Click(object sender, EventArgs e)
		{
			Response.Redirect("~/ManageGroups.aspx");
		}

		protected void btnMakeCall_Click(object sender, EventArgs e)
		{
			Response.Redirect("~/MakeCall.aspx");
		}

		protected void btnViewMessagesOnly_Click(object sender, EventArgs e)
		{
			Response.Redirect("~/ViewMessagesOnly.aspx");
		}

		protected void btnViewCallLog_Click(object sender, EventArgs e)
		{
			Response.Redirect("~/ViewCallLog.aspx");
		}

		protected void btnCallManagement_Click(object sender, EventArgs e)
		{
			Response.Redirect("~/CallManagement.aspx");
		}

		protected void btnMessageManagement_Click(object sender, EventArgs e)
		{
			Response.Redirect("~/MessageManagement.aspx");
		}
	}
}