﻿using CallManager.Prototype.DataLayer;
using RingCentral;
using RingCentral.Http;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CallManager.Prototype
{
	public partial class MessageManagement : System.Web.UI.Page
	{
		#region Global
		protected SDK ringsdk;
		#endregion


		#region Page Functions

		protected void Page_Init(Object sender, EventArgs e)
		{

			string RingCentralAPI_URL = ConfigurationManager.AppSettings["RingCentralAPI_URL"];
			string RingCentralAPI_Key = ConfigurationManager.AppSettings["RingCentralAPI_Key"];
			string RingCentralAPI_Secret = ConfigurationManager.AppSettings["RingCentralAPI_Secret"];
			string RingCentralAPI_AppName = ConfigurationManager.AppSettings["RingCentralAPI_AppName"];
			ringsdk = new SDK(RingCentralAPI_Key, RingCentralAPI_Secret, RingCentralAPI_URL, RingCentralAPI_AppName);


			//Request Refresh of AccessToken or re authenticate user
			if (!ringsdk.Platform.LoggedIn())
			{

				if (Session["RingAuthToken"] != null)
				{
					//Set Auth Data into SDK and refresh Access Token
					ringsdk.Platform.Auth.SetData((Dictionary<string, string>)Session["RingAuthToken"]);
					ringsdk.Platform.Refresh();
				}
				else
				{
					//We have no auth data so Request login (redirectects user to ring central login page)
					//after login - user will be redirected back to here with QueryState "AuthRequest"
					RequestAuthCodeFromRingCentral();
				}
			}
			else
			{
				//ringsdk.Platform.Refresh(); //done by Loggedin Check
			}

		}

		protected void Page_Load(object sender, EventArgs e)
		{
			if (!IsPostBack)
			{
				//Set default time frame
				txtFrom.Text = DateTime.Now.AddMonths(-3).ToString("yyyy-MM-dd");
				txtTo.Text = DateTime.Now.AddDays(1).ToString("yyyy-MM-dd");

				GetMessages();
			}
		}

		protected void btnDelete_Command(object sender, CommandEventArgs e)
		{
			if (e.CommandName == "DELETEMESSAGE")
			{
				string messageID = e.CommandArgument.ToString();
				DeleteMessage(messageID);
			}
		}

		/// <summary>
		/// Trigger Download of message
		/// I want to look into playing the message through a control on the page.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		protected void btnPlay_Command(object sender, CommandEventArgs e)
		{
			if (e.CommandName == "PLAYMESSAGE")
			{
				string url = e.CommandArgument.ToString();


				var request1 = new Request(url.Substring(url.IndexOf("restapi")));
				var response1 = ringsdk.Platform.Get(request1);
				if (response1.OK)
				{

					Task<byte[]> t = Task.Run(delegate
					{
						return response1.Response.Content.ReadAsByteArrayAsync();
					});

					t.Wait();

					Response.Clear();
					string fileName = "CallManagerDownload.wav";
					Response.AddHeader("content-disposition", "attachment; filename=" + fileName);


					string yourFileMimeType = "audio/x-wav";
					Response.ContentType = yourFileMimeType;


					Response.BinaryWrite(t.Result);
					Response.Flush();
					Response.End();

				}
				else
				{
					txtResponse.Text = "Error..";

				}


			}
		}

		protected void btnRefresh_Click(object sender, EventArgs e)
		{
			GetMessages();
			txtResponse.Text = String.Empty;
		}


		protected void gvMessageLog_RowDataBound(object sender, GridViewRowEventArgs e)
		{
			if (e.Row != null)
			{
				//var audioPlayer = e.Row.FindControl("audioPlayer");

			}
		}


		#endregion

		#region Helpers

		/// <summary>
		/// Redirect to Ring Central to Authenticate User
		/// Ring Central will return user to url of our choosing
		/// </summary>
		private void RequestAuthCodeFromRingCentral()
		{
			string RingCentralAPI_AuthCallBack = ConfigurationManager.AppSettings["RingCentralAPI_AuthCallBack"];

			string WebRequest_URL = ringsdk.Platform.AuthorizeUri(RingCentralAPI_AuthCallBack, "AuthRequest");

			Response.Redirect(WebRequest_URL);

		}

		/// <summary>
		/// Grab all messages for user and their groups and add them to the grid view
		/// </summary>
		private void GetMessages()
		{
			string extensionID = ringsdk.Platform.Auth.GetData()["owner_id"];
			List<MessageItem> DisplayMessages = new List<MessageItem>();

			//get time frame strings ready
			DateTime FromDate = DateTime.ParseExact("2017-01-01", "yyyy-MM-dd", null);
			string FromDatestr = FromDate.ToString("yyyy-MM-dd");

			if (DateTime.TryParseExact(txtFrom.Text, "yyyy-MM-dd", null, System.Globalization.DateTimeStyles.None, out FromDate))
			{
				FromDatestr = txtFrom.Text;
			}

			DateTime ToDate = DateTime.Now.AddDays(1);
			string ToDatestr = ToDate.ToString("yyyy-MM-dd");

			if (DateTime.TryParseExact(txtTo.Text, "yyyy-MM-dd", null, System.Globalization.DateTimeStyles.None, out ToDate))
			{
				ToDatestr = txtTo.Text;
			}

			//get a list of my groups
			List<data_Groups> SelectedGroups = new List<data_Groups>();

			using (var cmEntities = new DataLayer.CMEntities())
			{
				var groups = from ug in cmEntities.link_UserGroups where ug.data_Users.RingCentralExtensionID == extensionID select ug.data_Groups;
				SelectedGroups.AddRange(groups);

			}

			//get messages for user and add to list
			var request1 = new Request(String.Format("restapi/v1.0/account/~/extension/{0}/message-store?dateFrom={1}&dateTo={2}", extensionID, FromDatestr, ToDatestr));
			var response1 = ringsdk.Platform.Get(request1);
			if (response1.OK)
			{
				foreach (var record in response1.Json["records"].Children())
				{
					//note - there is a possible bug? in the to number json response - the recipient is returned inside an array when the from isn't - the call log doesnt have the same array.
					DisplayMessages.Add(new MessageItem
					{
						ID = record["id"].ToString(),
						URI = record["uri"].ToString(),
						ReadStatus = record["readStatus"].ToString(),
						Direction = record["direction"].ToString(),
						FromNumber = record["from"]["phoneNumber"] != null ? record["from"]["phoneNumber"].ToString() : record["from"]["name"].ToString(),
						ToNumber = record["to"].First()["phoneNumber"] != null ? record["to"].First()["phoneNumber"].ToString() : record["to"].First()["name"].ToString(),
						FromName = record["from"]["name"] != null ? record["from"]["name"].ToString() : String.Empty,
						ToName = record["to"].First()["name"] != null ? record["to"].First()["name"].ToString() : String.Empty,
						DateAndTime = (DateTime)record["creationTime"],
						AttachmentID = record["attachments"].First()["id"].ToString(),
						AttachmentURI = record["attachments"].First()["uri"].ToString(),
						AttachmentDuration = int.Parse(record["attachments"].First()["vmDuration"].ToString()),
						Type = record["type"].ToString(),
						Source = "Current User"
					});
				}



				//txtResponse.Text = response1.Body.ToString();
			}
			else
			{
				txtResponse.Text = "ERROR: " + response1.Body.ToString();
			}

			//loop though each groups messages and add to list
			foreach (var groupExtension in SelectedGroups)
			{
				var request2 = new Request(string.Format("restapi/v1.0/account/{0}/extension/{1}/message-store?dateFrom={2}&dateTo={3}", ConfigurationManager.AppSettings["RingCentralAPI_AccountNumber"], groupExtension.RingCentralGroupID, FromDatestr, ToDatestr));

				var response2 = ringsdk.Platform.Get(request2);
				if (response2.OK)
				{

					foreach (var record in response2.Json["records"].Children())
					{
						//if(record[""])
						DisplayMessages.Add(new MessageItem
						{

							ID = record["id"].ToString(),
							URI = record["uri"].ToString(),
							ReadStatus = record["readStatus"].ToString(),
							Direction = record["direction"].ToString(),
							FromNumber = record["from"]["phoneNumber"] != null ? record["from"]["phoneNumber"].ToString() : record["from"]["name"].ToString(),
							ToNumber = record["to"].First()["phoneNumber"] != null ? record["to"].First()["phoneNumber"].ToString() : record["to"].First()["name"].ToString(),
							FromName = record["from"]["name"] != null ? record["from"]["name"].ToString() : String.Empty,
							ToName = record["to"].First()["name"] != null ? record["to"].First()["name"].ToString() : String.Empty,
							DateAndTime = (DateTime)record["creationTime"],
							AttachmentID = record["attachments"].First()["id"].ToString(),
							AttachmentURI = record["attachments"].First()["uri"].ToString(),
							AttachmentDuration = int.Parse(record["attachments"].First()["vmDuration"].ToString()),
							Type = record["type"].ToString(),
							Source = groupExtension.Name
						});
					}

					//txtResponse.Text += response2.Body.ToString() + "<br />NEXT<BR /><BR />";
				}
				else
				{
					txtResponse.Text += "ERROR: " + response2.Body.ToString() + "<br />NEXT<BR /><BR />";
				}

			}

			//DisplayMessages.sort

			gvMessageLog.DataSource = DisplayMessages;
			gvMessageLog.DataBind();

		}

		private void DeleteMessage(string messageID)
		{

			//DELETE /restapi/v1.0/account/1346632010/extension/1346632010/message-store/315433012010 HTTP/1.1
			string extensionID = ringsdk.Platform.Auth.GetData()["owner_id"];
			List<MessageItem> DisplayMessages = new List<MessageItem>();


			var request1 = new Request(String.Format("restapi/v1.0/account/~/extension/{0}/message-store/{1}", extensionID, messageID));
			var response1 = ringsdk.Platform.Delete(request1);
			if (response1.OK)
			{
				txtResponse.Text = response1.Body.ToString() + "<BR />YOU WILL NEED TO MANUALLY REFRESH NOW.";//temp
			}
			else
			{
				txtResponse.Text = "ERROR: " + response1.Body.ToString();
			}

		}

		#endregion
	}

	public class MessageItem
	{
		public string ID { get; set; }
		public string URI { get; set; }
		public string ReadStatus { get; set; }
		public string Direction { get; set; }
		public string FromNumber { get; set; }
		public string ToNumber { get; set; }
		public string FromName { get; set; }
		public string ToName { get; set; }
		public DateTime DateAndTime { get; set; }
		public string AttachmentID { get; set; }
		public string AttachmentURI { get; set; }
		public int AttachmentDuration { get; set; }
		public string Type { get; set; }
		public string Source { get; set; }
		public string PlayJS { get { return "window.open(\"" + AttachmentURI + "\", \"_blank\"); "; } }
		//$window.open("<%# Eval("AttachmentURI") %>", "_blank");
		//OnClientClick='<%# Eval("PlayJS") %>'
	}

}