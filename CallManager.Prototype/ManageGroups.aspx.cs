﻿using RingCentral;
using RingCentral.Http;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CallManager.Prototype
{
	public partial class ManageGroups : System.Web.UI.Page
	{

		#region Global
		protected SDK ringsdk;
		#endregion


		#region Page Functions

		protected void Page_Init(Object sender, EventArgs e)
		{

			string RingCentralAPI_URL = ConfigurationManager.AppSettings["RingCentralAPI_URL"];
			string RingCentralAPI_Key = ConfigurationManager.AppSettings["RingCentralAPI_Key"];
			string RingCentralAPI_Secret = ConfigurationManager.AppSettings["RingCentralAPI_Secret"];
			string RingCentralAPI_AppName = ConfigurationManager.AppSettings["RingCentralAPI_AppName"];
			ringsdk = new SDK(RingCentralAPI_Key, RingCentralAPI_Secret, RingCentralAPI_URL, RingCentralAPI_AppName);


			//Request Refresh of AccessToken or re authenticate user
			if (!ringsdk.Platform.LoggedIn())
			{

				if (Session["RingAuthToken"] != null)
				{
					//Set Auth Data into SDK and refresh Access Token
					ringsdk.Platform.Auth.SetData((Dictionary<string, string>)Session["RingAuthToken"]);
					ringsdk.Platform.Refresh();
				}
				else
				{
					//We have no auth data so Request login (redirectects user to ring central login page)
					//after login - user will be redirected back to here with QueryState "AuthRequest"
					RequestAuthCodeFromRingCentral();
				}
			}
			else
			{
				//ringsdk.Platform.Refresh(); //done by Loggedin Check
			}

		}

		protected void Page_Load(object sender, EventArgs e)
		{
			if (!IsPostBack)
			{
				UpdateMasterGroups();
				LoadGroups();
			}
		}

		protected void btnSave_Click(object sender, EventArgs e)
		{
			SaveChanges();
		}

		protected void gvGroups_RowDataBound(object sender, GridViewRowEventArgs e)
		{
			if (e.Row.RowType == DataControlRowType.DataRow)
			{
				CheckBox chkSelected = (CheckBox)e.Row.FindControl("chkIncludeGroup");
				HiddenField hdnID = (HiddenField)e.Row.FindControl("hdnID");

				chkSelected.Checked = ((GroupItem)e.Row.DataItem).Selected;
				hdnID.Value = ((GroupItem)e.Row.DataItem).GroupID.ToString();
			}
		}
		#endregion


		#region Helpers

		/// <summary>
		/// Redirect to Ring Central to Authenticate User
		/// Ring Central will return user to url of our choosing
		/// </summary>
		private void RequestAuthCodeFromRingCentral()
		{
			string RingCentralAPI_AuthCallBack = ConfigurationManager.AppSettings["RingCentralAPI_AuthCallBack"];

			string WebRequest_URL = ringsdk.Platform.AuthorizeUri(RingCentralAPI_AuthCallBack, "AuthRequest");

			Response.Redirect(WebRequest_URL);

		}

		/// <summary>
		/// Update the table which lists all groups in ring central
		/// </summary>
		private void UpdateMasterGroups()
		{
			string extensionID = ringsdk.Platform.Auth.GetData()["owner_id"];

			List<GroupItem> ringGroups = new List<GroupItem>();

			//Get a list of all groups from RingCentral
			var request1 = new Request(String.Format("/restapi/v1.0/account/168732004/extension?type=Department", extensionID));
			var response1 = ringsdk.Platform.Get(request1);
			if (response1.OK)
			{
				foreach (var ringGroupRecord in response1.Json["records"])
				{
					ringGroups.Add(new GroupItem { RingCentralGrouplID = ringGroupRecord["id"].ToString(), GroupID = Guid.Empty, GroupExtension = ringGroupRecord["extensionNumber"].ToString(), GroupName = ringGroupRecord["name"].ToString(), Selected = false });
				}
			}
			else
			{
				lblResults.Text = "An Error has Occured Loading Groups - " + response1.Error;
			}

			//Get a list of all locally known groups
			List<DataLayer.data_Groups> savedGroups;
			using (var cmEntities = new DataLayer.CMEntities())
			{
				var sgroups = from g in cmEntities.data_Groups select g;
				savedGroups = sgroups.ToList();


				var newGroups = from rg in ringGroups where !(from sg in savedGroups select sg.RingCentralGroupID).Contains(rg.RingCentralGrouplID) select rg;
				var delGroups = from sg in savedGroups where !(from rg in ringGroups select rg.RingCentralGrouplID).Contains(sg.RingCentralGroupID) select sg;
				var updGroups = from sg in savedGroups where (from rg in ringGroups select rg.RingCentralGrouplID).Contains(sg.RingCentralGroupID) select sg;

				foreach (var item in newGroups)
				{
					var newGroup = cmEntities.data_Groups.Create();
					newGroup.ID = Guid.NewGuid();
					newGroup.Name = item.GroupName;
					newGroup.RingCentralGroupID = item.RingCentralGrouplID;
					newGroup.Extension = item.GroupExtension;
					cmEntities.data_Groups.Add(newGroup);
				}

				foreach (var item in delGroups)
				{
					cmEntities.data_Groups.Remove(item);
				}

				foreach (var item in updGroups)
				{
					var ringItem = (from rg in ringGroups where rg.RingCentralGrouplID == item.RingCentralGroupID select rg).FirstOrDefault();
					if (ringItem != null)
					{
						item.Name = ringItem.GroupName;
						item.Extension = ringItem.GroupExtension;
					}
				}

				cmEntities.SaveChanges();
			}
		}

		/// <summary>
		/// Load all groups into the GridView and check the ones which the current user has saved against themselves in the link_UserGroups table
		/// </summary>
		private void LoadGroups()
		{
			string extensionID = ringsdk.Platform.Auth.GetData()["owner_id"];

			//Get my already selected groups from Database and mark them as enabled / disabled
			List<GroupItem> displayGroups = new List<GroupItem>();
			using (var cmEntities = new DataLayer.CMEntities())
			{
				var allGroups = (from g in cmEntities.data_Groups select g).ToList();
				var selectedGroups = (from ug in cmEntities.link_UserGroups where ug.data_Users.RingCentralExtensionID == extensionID select ug.data_Groups).ToList();

				foreach (var item in allGroups.OrderBy(x => x.Name))
				{
					displayGroups.Add(new GroupItem
					{
						GroupID = item.ID,
						RingCentralGrouplID = item.RingCentralGroupID,
						GroupExtension = item.Extension,
						GroupName = item.Name,
						Selected = selectedGroups.Exists(x => x.RingCentralGroupID == item.RingCentralGroupID)
					});
				}

				gvGroups.DataSource = displayGroups;
				gvGroups.DataBind();

			}
		}

		/// <summary>
		/// Loop through each record, work out whether its an add / delete / ignore for the link table and act appropriatly
		/// </summary>
		private void SaveChanges()
		{
			List<string> selectedIDs = new List<string>();
			//Get Selected ID's from list
			foreach (GridViewRow row in gvGroups.Rows)
			{
				if (row.RowType == DataControlRowType.DataRow)
				{
					CheckBox chkSelected = (CheckBox)row.FindControl("chkIncludeGroup");
					HiddenField hdnID = (HiddenField)row.FindControl("hdnID");

					if (chkSelected.Checked)
					{
						selectedIDs.Add(hdnID.Value);
					}
				}
			}

			string extensionID = ringsdk.Platform.Auth.GetData()["owner_id"];

			using (var cmEntities = new DataLayer.CMEntities())
			{
				var existingLinks = from ug in cmEntities.link_UserGroups where ug.data_Users.RingCentralExtensionID == extensionID select ug;

				//remove any links which no longer apply
				foreach (var remLink in existingLinks)
				{
					if (!selectedIDs.Contains(remLink.GroupID.ToString()))
					{
						cmEntities.link_UserGroups.Remove(remLink);
					}
				}

				//add new links
				foreach (var addLink in selectedIDs)
				{
					if (!existingLinks.Select(x => x.GroupID).Contains(Guid.Parse(addLink)))
					{
						var userID = (from u in cmEntities.data_Users where u.RingCentralExtensionID == extensionID select u.ID).FirstOrDefault();

						cmEntities.link_UserGroups.Add(new DataLayer.link_UserGroups
						{
							ID = Guid.NewGuid(),
							GroupID = Guid.Parse(addLink),
							UserID = userID
						});
					}
				}
				cmEntities.SaveChanges();
			}
		}
		#endregion

	}

	public class GroupItem
	{
		public Guid GroupID { get; set; }
		public string RingCentralGrouplID { get; set; }
		public string GroupName { get; set; }
		public string GroupExtension { get; set; }
		public Boolean Selected { get; set; }
	}
}