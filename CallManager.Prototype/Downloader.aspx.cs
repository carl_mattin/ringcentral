﻿using RingCentral;
using RingCentral.Http;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CallManager.Prototype
{
	public partial class Downloader : System.Web.UI.Page
	{
		#region Global
		protected SDK ringsdk;
		#endregion


		#region Page Functions

		protected void Page_Init(Object sender, EventArgs e)
		{

			string RingCentralAPI_URL = ConfigurationManager.AppSettings["RingCentralAPI_URL"];
			string RingCentralAPI_Key = ConfigurationManager.AppSettings["RingCentralAPI_Key"];
			string RingCentralAPI_Secret = ConfigurationManager.AppSettings["RingCentralAPI_Secret"];
			string RingCentralAPI_AppName = ConfigurationManager.AppSettings["RingCentralAPI_AppName"];
			ringsdk = new SDK(RingCentralAPI_Key, RingCentralAPI_Secret, RingCentralAPI_URL, RingCentralAPI_AppName);


			//Request Refresh of AccessToken or re authenticate user
			if (!ringsdk.Platform.LoggedIn())
			{

				if (Session["RingAuthToken"] != null)
				{
					//Set Auth Data into SDK and refresh Access Token
					ringsdk.Platform.Auth.SetData((Dictionary<string, string>)Session["RingAuthToken"]);
					ringsdk.Platform.Refresh();
				}
				else
				{
					//We have no auth data so Request login (redirectects user to ring central login page)
					//after login - user will be redirected back to here with QueryState "AuthRequest"
					RequestAuthCodeFromRingCentral();
				}
			}
			else
			{
				//ringsdk.Platform.Refresh(); //done by Loggedin Check
			}

		}

		protected void Page_Load(object sender, EventArgs e)
		{
			string url = Page.Request.QueryString["path"];
			//eg - https://media.devtest.ringcentral.com/restapi/v1.0/account/168732004/extension/168732004/message-store/2801071004/content/2801071004
			if (url.StartsWith("https://media.") && url.Contains("content"))
			{
				//basic checking to make sure this isn't being abused
				//need a better player anyway

				var request1 = new Request(url.Substring(url.IndexOf("restapi")));
				var response1 = ringsdk.Platform.Get(request1);
				if (response1.OK)
				{
					
					Task<byte[]> t = Task.Run(delegate
					{
						return response1.Response.Content.ReadAsByteArrayAsync();
					});

					t.Wait();

					
					string fileName = "CallManagerDownload.wav";
					Response.AddHeader("content-disposition", "attachment; filename=" + fileName);


					string yourFileMimeType = "audio/x-wav";
					Response.ContentType = yourFileMimeType;


					Response.Write(t.Result);


				}
				else
				{
					lblResponse.Text = "Error..";

				}




			}
		}

			#endregion


			#region Helpers

			/// <summary>
			/// Redirect to Ring Central to Authenticate User
			/// Ring Central will return user to url of our choosing
			/// </summary>
			/// <summary>
			/// Redirect to Ring Central to Authenticate User
			/// Ring Central will return user to url of our choosing
			/// </summary>
			private void RequestAuthCodeFromRingCentral()
			{
				string RingCentralAPI_AuthCallBack = ConfigurationManager.AppSettings["RingCentralAPI_AuthCallBack"];

				string WebRequest_URL = ringsdk.Platform.AuthorizeUri(RingCentralAPI_AuthCallBack, "AuthRequest");

				Response.Redirect(WebRequest_URL);

			}

			#endregion

		}
}