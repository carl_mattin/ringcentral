﻿using RingCentral;
using RingCentral.Http;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CallManager.Prototype
{
	public partial class ViewMessagesOnly : System.Web.UI.Page
	{

		#region Global
		protected SDK ringsdk;
		#endregion


		#region Page Functions

		protected void Page_Init(Object sender, EventArgs e)
		{

			string RingCentralAPI_URL = ConfigurationManager.AppSettings["RingCentralAPI_URL"];
			string RingCentralAPI_Key = ConfigurationManager.AppSettings["RingCentralAPI_Key"];
			string RingCentralAPI_Secret = ConfigurationManager.AppSettings["RingCentralAPI_Secret"];
			string RingCentralAPI_AppName = ConfigurationManager.AppSettings["RingCentralAPI_AppName"];
			ringsdk = new SDK(RingCentralAPI_Key, RingCentralAPI_Secret, RingCentralAPI_URL, RingCentralAPI_AppName);


			//Request Refresh of AccessToken or re authenticate user
			if (!ringsdk.Platform.LoggedIn())
			{

				if (Session["RingAuthToken"] != null)
				{
					//Set Auth Data into SDK and refresh Access Token
					ringsdk.Platform.Auth.SetData((Dictionary<string, string>)Session["RingAuthToken"]);
					ringsdk.Platform.Refresh();
				}
				else
				{
					//We have no auth data so Request login (redirectects user to ring central login page)
					//after login - user will be redirected back to here with QueryState "AuthRequest"
					RequestAuthCodeFromRingCentral();
				}
			}
			else
			{
				//ringsdk.Platform.Refresh(); //done by Loggedin Check
			}

		}

		protected void Page_Load(object sender, EventArgs e)
		{
			if (!IsPostBack)
			{
				GetMessages();
			}
		}

		#endregion


		#region Helpers

		/// <summary>
		/// Redirect to Ring Central to Authenticate User
		/// Ring Central will return user to url of our choosing
		/// </summary>
		private void RequestAuthCodeFromRingCentral()
		{
			string RingCentralAPI_AuthCallBack = ConfigurationManager.AppSettings["RingCentralAPI_AuthCallBack"];

			string WebRequest_URL = ringsdk.Platform.AuthorizeUri(RingCentralAPI_AuthCallBack, "AuthRequest");

			Response.Redirect(WebRequest_URL);

		}


		private void GetMessages()
		{
			string extensionID = ringsdk.Platform.Auth.GetData()["owner_id"];
			List<Message> DisplayMessages = new List<Message>();


			//var request1 = new Request(String.Format("restapi/v1.0/account/~/extension/{0}/message-store?dateFrom=2017-01-01", extensionID));

			var request1 = new Request(String.Format("restapi/v1.0/account/168732004/extension/169513004,169512004/message-store?dateFrom=2017-01-01"));

			var response1 = ringsdk.Platform.Get(request1);
			if (response1.OK)
			{
				foreach (var record in response1.Json["records"].Children())
				{
					//note - there is a possible bug? in the to number json response - the recipient is returned inside an array when the from isn't - the call log doesnt have the same array.
					DisplayMessages.Add(new Message
					{
						ID = record["id"].ToString(),
						FromNumber = record["from"]["phoneNumber"] != null ? record["from"]["phoneNumber"].ToString() : record["from"]["name"].ToString(),
						ToNumber = record["to"].First()["phoneNumber"] != null ? record["to"].First()["phoneNumber"].ToString() : record["to"].First()["name"].ToString(),
						Type = record["type"].ToString(),
						ReadStatus = record["readStatus"].ToString(),
						Direction = record["direction"].ToString(),
						uri = record["attachments"].First()["uri"].ToString()
					});
				}

				gvMessageLog.DataSource = DisplayMessages;
				gvMessageLog.DataBind();


				txtResponse.Text = response1.Body.ToString();
			}
			else
			{
				txtResponse.Text = "ERROR: " + response1.Body.ToString();
			}





		}



		public class Message
		{
			public string ID { get; set; }
			public string FromNumber { get; set; }
			public string ToNumber { get; set; }
			public string Type { get; set; }
			public string ReadStatus { get; set; }
			public string Direction { get; set; }
			public string uri { get; set; }

		}

		#endregion
		

		private void DeleteMessage(string messageID)
		{

			//DELETE /restapi/v1.0/account/1346632010/extension/1346632010/message-store/315433012010 HTTP/1.1
			string extensionID = ringsdk.Platform.Auth.GetData()["owner_id"];
			List<Message> DisplayMessages = new List<Message>();


			var request1 = new Request(String.Format("restapi/v1.0/account/~/extension/{0}/message-store/{1}", extensionID, messageID));
			var response1 = ringsdk.Platform.Delete(request1);
			if (response1.OK)
			{
				txtResponse.Text = response1.Body.ToString() + "<BR />YOU WILL NEED TO MANUALLY REFRESH NOW.";
			}
			else
			{
				txtResponse.Text = "ERROR: " + response1.Body.ToString();
			}

		}

		protected void btnDelete_Command(object sender, CommandEventArgs e)
		{
			if (e.CommandName == "DELETE")
			{
				string messageID = e.CommandArgument.ToString();
				DeleteMessage(messageID);
			}
		}
	}
}