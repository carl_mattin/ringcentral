﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="ViewCallLog.aspx.cs" Inherits="CallManager.Prototype.ViewCallLog" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
	
	Call Log 
	<asp:GridView ID="gvCallLog" runat="server" AutoGenerateColumns="false">
		<Columns>
			<asp:BoundField HeaderText="ID" DataField="ID" />
			<asp:BoundField HeaderText="FromNumber" DataField="FromNumber" />
			<asp:BoundField HeaderText="ToNumber" DataField="ToNumber" />
			<asp:BoundField HeaderText="Direction" DataField="Direction" />
			<asp:BoundField HeaderText="Result" DataField="Result" />
			<asp:BoundField HeaderText="Legs" DataField="Legs" />
			<asp:BoundField HeaderText="MessageURI" DataField="VoiceMessageID" />
		</Columns>

	</asp:GridView>
	<asp:Label ID="txtResponse" runat="server"></asp:Label>

</asp:Content>
