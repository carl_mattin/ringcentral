﻿using CallManager.Prototype.DataLayer;
using RingCentral;
using RingCentral.Http;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CallManager.Prototype
{
	public partial class ViewCallLog : System.Web.UI.Page
	{

		#region Global
		protected SDK ringsdk;
		#endregion


		#region Page Functions

		protected void Page_Init(Object sender, EventArgs e)
		{

			string RingCentralAPI_URL = ConfigurationManager.AppSettings["RingCentralAPI_URL"];
			string RingCentralAPI_Key = ConfigurationManager.AppSettings["RingCentralAPI_Key"];
			string RingCentralAPI_Secret = ConfigurationManager.AppSettings["RingCentralAPI_Secret"];
			string RingCentralAPI_AppName = ConfigurationManager.AppSettings["RingCentralAPI_AppName"];
			ringsdk = new SDK(RingCentralAPI_Key, RingCentralAPI_Secret, RingCentralAPI_URL, RingCentralAPI_AppName);


			//Request Refresh of AccessToken or re authenticate user
			if (!ringsdk.Platform.LoggedIn())
			{

				if (Session["RingAuthToken"] != null)
				{
					//Set Auth Data into SDK and refresh Access Token
					ringsdk.Platform.Auth.SetData((Dictionary<string, string>)Session["RingAuthToken"]);
					ringsdk.Platform.Refresh();
				}
				else
				{
					//We have no auth data so Request login (redirectects user to ring central login page)
					//after login - user will be redirected back to here with QueryState "AuthRequest"
					RequestAuthCodeFromRingCentral();
				}
			}
			else
			{
				//ringsdk.Platform.Refresh(); //done by Loggedin Check
			}

		}

		protected void Page_Load(object sender, EventArgs e)
		{
			if (!IsPostBack)
			{
				GetCallLog();
			}
		}

		#endregion


		#region Helpers

		/// <summary>
		/// Redirect to Ring Central to Authenticate User
		/// Ring Central will return user to url of our choosing
		/// </summary>
		private void RequestAuthCodeFromRingCentral()
		{
			string RingCentralAPI_AuthCallBack = ConfigurationManager.AppSettings["RingCentralAPI_AuthCallBack"];

			string WebRequest_URL = ringsdk.Platform.AuthorizeUri(RingCentralAPI_AuthCallBack, "AuthRequest");

			Response.Redirect(WebRequest_URL);

		}

		private void GetCallLog()
		{
			string extensionID = ringsdk.Platform.Auth.GetData()["owner_id"];
			List<data_Groups> SelectedGroups = new List<data_Groups>();

			using (var cmEntities = new DataLayer.CMEntities())
			{
				var groups = from ug in cmEntities.link_UserGroups where ug.data_Users.RingCentralExtensionID == extensionID select ug.data_Groups;
				SelectedGroups.AddRange(groups);

			}
			List<Call> DisplayCalls = new List<Call>();
			txtResponse.Text = String.Empty;

			//Get This Users Call Log

			//var request1 = new Request("/restapi/v1.0/account/168732004/call-log?view=Detailed&withRecording=true&showBlocked=true&dateFrom=2017-01-01");			//Requireds - ReadCompanyCallLog - which doesnt appear in the list of available permissions
			var request1 = new Request("/restapi/v1.0/account/168732004/call-log?view=Detailed&showBlocked=true&dateFrom=2017-01-01");			//Requireds - ReadCompanyCallLog - which doesnt appear in the list of available permissions
																																						
			//var request1 = new Request("/restapi/v1.0/account/~/call-log?view=Detailed&withRecording=true&showBlocked=true&dateFrom=2017-01-01");//Requireds - ReadCompanyCallLog - which doesnt appear in the list of available permissions

			//var request1 = new Request(string.Format("/restapi/v1.0/account/~/extension/{0}/call-log?view=Detailed&showBlocked=true", extensionID)); //Success User only
			//var request1 = new Request(string.Format("/restapi/v1.0/account/{0}/extension/{1}/call-log?view=Detailed&showBlocked=true&dateFrom=2017-01-01", ConfigurationManager.AppSettings["RingCentralAPI_AccountNumber"], extensionID));

			var response1 = ringsdk.Platform.Get(request1);
			if (response1.OK)
			{

				foreach (var record in response1.Json["records"].Children())
				{
					string MessageURI = String.Empty;
					foreach (var leg in record["legs"])
					{
						if (leg["message"] != null && leg["message"]["uri"] != null)
						{
							MessageURI = leg["message"]["uri"].ToString();
						}
					}

					DisplayCalls.Add(new Call
					{
						ID = record["id"].ToString(),
						FromNumber = record["from"]["phoneNumber"] != null ? record["from"]["phoneNumber"].ToString() : record["from"]["extensionNumber"].ToString(),
						ToNumber = record["to"]["phoneNumber"] != null ? record["to"]["phoneNumber"].ToString() : record["to"]["extensionNumber"].ToString(),
						Direction = record["direction"].ToString(),
						Result = record["result"].ToString(),
						Legs = record["legs"].Count(),
						VoiceMessageID = MessageURI,
						Source = "Current User"
					});
				}

				txtResponse.Text += response1.Body.ToString() + "<br />NEXT<BR /><BR />";
			}
			else
			{
				txtResponse.Text += "ERROR: " + response1.Body.ToString() + "<br />NEXT<BR /><BR />";
			}


			//foreach (var groupExtension in SelectedGroups)
			//{
			//	var request2 = new Request(string.Format("/restapi/v1.0/account/{0}/extension/{1}/call-log?view=Detailed&showBlocked=true&dateFrom=2017-01-01", ConfigurationManager.AppSettings["RingCentralAPI_AccountNumber"], groupExtension.RingCentralGroupID));

			//	var response2 = ringsdk.Platform.Get(request2);
			//	if (response2.OK)
			//	{

			//		foreach (var record in response2.Json["records"].Children())
			//		{
			//			//if(record[""])
			//			DisplayCalls.Add(new Call
			//			{
			//				ID = record["id"].ToString(),
			//				FromNumber = record["from"]["phoneNumber"] != null ? record["from"]["phoneNumber"].ToString() : record["from"]["extensionNumber"].ToString(),
			//				ToNumber = record["to"]["phoneNumber"] != null ? record["to"]["phoneNumber"].ToString() : record["to"]["extensionNumber"].ToString(),
			//				Direction = record["direction"].ToString(),
			//				Result = record["result"].ToString(),
			//				Legs = record["legs"].Count(),
			//				VoiceMessageID = "",
			//				Source = groupExtension.Name
			//			});
			//		}

			//		txtResponse.Text += response2.Body.ToString() + "<br />NEXT<BR /><BR />";
			//	}
			//	else
			//	{
			//		txtResponse.Text += "ERROR: " + response2.Body.ToString() + "<br />NEXT<BR /><BR />";
			//	}

			//}

			gvCallLog.DataSource = DisplayCalls;
			gvCallLog.DataBind();

		}
		#endregion


		public class Call
		{
			public string ID { get; set; }
			public string FromNumber { get; set; }
			public string ToNumber { get; set; }
			public string Direction { get; set; }
			public string Result { get; set; }
			public int Legs { get; set; }
			public string VoiceMessageID { get; set; }
			public string Source { get; set; }

		}
	}
}