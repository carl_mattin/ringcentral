﻿using Newtonsoft.Json;
using RingCentral;
using RingCentral.Http;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CallManager.Prototype
{
	public partial class MakeCall : System.Web.UI.Page
	{

		#region Global
		protected SDK ringsdk;
		#endregion


		#region Page Functions

		protected void Page_Init(Object sender, EventArgs e)
		{

			string RingCentralAPI_URL = ConfigurationManager.AppSettings["RingCentralAPI_URL"];
			string RingCentralAPI_Key = ConfigurationManager.AppSettings["RingCentralAPI_Key"];
			string RingCentralAPI_Secret = ConfigurationManager.AppSettings["RingCentralAPI_Secret"];
			string RingCentralAPI_AppName = ConfigurationManager.AppSettings["RingCentralAPI_AppName"];
			ringsdk = new SDK(RingCentralAPI_Key, RingCentralAPI_Secret, RingCentralAPI_URL, RingCentralAPI_AppName);


			//Request Refresh of AccessToken or re authenticate user
			if (!ringsdk.Platform.LoggedIn())
			{

				if (Session["RingAuthToken"] != null)
				{
					//Set Auth Data into SDK and refresh Access Token
					ringsdk.Platform.Auth.SetData((Dictionary<string, string>)Session["RingAuthToken"]);
					ringsdk.Platform.Refresh();
				}
				else
				{
					//We have no auth data so Request login (redirectects user to ring central login page)
					//after login - user will be redirected back to here with QueryState "AuthRequest"
					RequestAuthCodeFromRingCentral();
				}
			}
			else
			{
				//ringsdk.Platform.Refresh(); //done by Loggedin Check
			}

		}

		protected void Page_Load(object sender, EventArgs e)
		{
			if (!IsPostBack)
			{
				LoadGroups();
			}
		}

		protected void btnMakeCall_Click(object sender, EventArgs e)
		{
			MakeRingCentralCall();
		}


		#endregion


		#region Helpers

		/// <summary>
		/// Redirect to Ring Central to Authenticate User
		/// Ring Central will return user to url of our choosing
		/// </summary>
		private void RequestAuthCodeFromRingCentral()
		{
			string RingCentralAPI_AuthCallBack = ConfigurationManager.AppSettings["RingCentralAPI_AuthCallBack"];

			string WebRequest_URL = ringsdk.Platform.AuthorizeUri(RingCentralAPI_AuthCallBack, "AuthRequest");

			Response.Redirect(WebRequest_URL);

		}

		private void LoadGroups()
		{
			string extensionID = ringsdk.Platform.Auth.GetData()["owner_id"];

			using (var cmEntities = new DataLayer.CMEntities())
			{
				var groups = from ug in cmEntities.link_UserGroups where ug.data_Users.RingCentralExtensionID == extensionID select ug.data_Groups;
				ddlCallFrom.DataSource = groups.ToList();
				ddlCallFrom.DataTextField = "Extension";
				ddlCallFrom.DataValueField = "RingCentralGroupID";
				ddlCallFrom.DataBind();
			}
			
			
		}

		private void MakeRingCentralCall()
		{
			string extensionID = ringsdk.Platform.Auth.GetData()["owner_id"];

			var testfrom1 = new { phoneNumber = txtCallTo.Text };
			var testfrom2 = new { forwardingNumberId = ddlCallFrom.SelectedValue };
			var testfrom3 = new { phoneNumber = "1" };
			var testfrom4 = new { forwardingNumberId = "1" };

			//Get a list of all groups from RingCentral
			var requestBody = new
			{
				country = new { id = "44" },
				//from = new { forwardingNumberId = ddlCallFrom.SelectedValue},//Not Working yet- low priority
				from = new { phoneNumber = testfrom4 },
				to = new { phoneNumber = txtCallTo.Text },
				playPrompt = true
				
			};

			var request1 = new Request(String.Format("/restapi/v1.0/account/{0}/extension/{1}/ringout", ConfigurationManager.AppSettings["RingCentralAPI_AccountNumber"], extensionID), JsonConvert.SerializeObject(requestBody));
			var response1 = ringsdk.Platform.Post(request1);
			if (response1.OK)
			{
				lblResults.Text = "Your phone should be dialling now.";
			}
			else
			{
				lblResults.Text = "An Error has Occured Loading Groups - " + response1.Error;
			}
		}

		#endregion
	}
}