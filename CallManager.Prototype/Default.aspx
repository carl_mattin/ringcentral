﻿<%@ Page Title="Call Manager" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="CallManager.Prototype._Default" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">

	<div>
		<div>
			<h2>Part 4 - Final Layout</h2>
			<p>
				<asp:Label ID="Label1" runat="server" Text="Manage my Groups:"></asp:Label>
				<asp:Button ID="Button1" runat="server" Text="Manage my Groups" OnClick="btnManageGroups_Click" />
				<br />
				<br />
				<asp:Label ID="lblCallManagement" runat="server" Text="Call Management:"></asp:Label>
				<asp:Button ID="btnCallManagement" runat="server" Text="Call Management" OnClick="btnCallManagement_Click" />
				<br />
				<br />
				<asp:Label ID="lblMessageManagement" runat="server" Text="Message Management:"></asp:Label>
				<asp:Button ID="btnMessageManagement" runat="server" Text="Message Management" OnClick="btnMessageManagement_Click" />
				<br />
				<br />
			</p>
		</div>
	</div>

	<div>

		<div>
			<h2>Part 3 - Page Buttons</h2>
			<p>
				<asp:Label ID="lblManageGroups" runat="server" Text="Manage my Groups:"></asp:Label>
				<asp:Button ID="btnManageGroups" runat="server" Text="Manage my Groups" OnClick="btnManageGroups_Click" />
				<br />
				<br />
				<asp:Label ID="lblMakeCall" runat="server" Text="Make a Call:"></asp:Label>
				<asp:Button ID="btnMakeCall" runat="server" Text="Make a Call" OnClick="btnMakeCall_Click" />
				<br />
				<br />
				<asp:Label ID="lblViewMessagesOnly" runat="server" Text="View Messages Only:"></asp:Label>
				<asp:Button ID="btnViewMessagesOnly" runat="server" Text="View Messages Only" OnClick="btnViewMessagesOnly_Click" />
				<br />
				<br />
				<asp:Label ID="lblViewCallLog" runat="server" Text="View Call Log:"></asp:Label>
				<asp:Button ID="btnViewCallLog" runat="server" Text="View Call Log" OnClick="btnViewCallLog_Click" />
				<br />
				<br />
			</p>
		</div>
	</div>

	<div>

		<div>
			<h2>Part 2 - Debug</h2>
			<p>
				<asp:TextBox ID="txtSent" runat="server" TextMode="MultiLine" Rows="10" Width="1000"></asp:TextBox>
				--
                <asp:TextBox ID="txtResponse" runat="server" TextMode="MultiLine" Rows="10" Width="1000"></asp:TextBox>
			</p>
		</div>
	</div>
	<div class="row">
		<div class="col-md-4">
			<h2>Part 1 - Buttons!!!!!</h2>
			<p>
				<asp:Label ID="lblCurrentUserInfo" runat="server" Text="Update Current User Information:"></asp:Label>
				<asp:Button ID="btnCurrentUserInfo" runat="server" Text="Update User Information" OnClick="btnCurrentUserInfo_Click" />
				<br />
				<br />
				<asp:Label ID="lblGetGroupList" runat="server" Text="Get Group List:"></asp:Label>
				<asp:Button ID="btnGetGroupList" runat="server" Text="Get Group List" OnClick="btnGetGroupList_Click" />
				<br />
				<br />
				<asp:Label ID="lblGetCallLog" runat="server" Text="Get Users Call Log:"></asp:Label>
				<asp:Button ID="btnGetCallLog" runat="server" Text="Get Call Log" OnClick="btnGetCallLog_Click" />
				<br />
				<br />
				<asp:Label ID="lblGetGroupCallLog" runat="server" Text="Get Group Call Log:"></asp:Label>
				<asp:Button ID="btnGetGroupCallLog" runat="server" Text="Get Call Log" OnClick="btnGetGroupCallLog_Click" />
				<br />
				<br />
				<asp:Label ID="lblGetCallMessages" runat="server" Text="Get Users Call Messages:"></asp:Label>
				<asp:Button ID="btnGetCallMessages" runat="server" Text="Get Call Messages" OnClick="btnGetCallMessages_Click" />
				<br />
				<br />
				<asp:Label ID="lblGetGroupMessages" runat="server" Text="Get Group Call Messages:"></asp:Label>
				<asp:Button ID="btnGetGroupMessages" runat="server" Text="Get Call Messages" OnClick="btnGetGroupMessages_Click" />
			</p>
		</div>
	</div>

</asp:Content>
