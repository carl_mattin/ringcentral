﻿using CallManager.Prototype.DataLayer;
using RingCentral;
using RingCentral.Http;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CallManager.Prototype
{
	public partial class CallManagement : System.Web.UI.Page
	{

		#region Global
		protected SDK ringsdk;
		#endregion

		#region Page Functions

		protected void Page_Init(Object sender, EventArgs e)
		{

			string RingCentralAPI_URL = ConfigurationManager.AppSettings["RingCentralAPI_URL"];
			string RingCentralAPI_Key = ConfigurationManager.AppSettings["RingCentralAPI_Key"];
			string RingCentralAPI_Secret = ConfigurationManager.AppSettings["RingCentralAPI_Secret"];
			string RingCentralAPI_AppName = ConfigurationManager.AppSettings["RingCentralAPI_AppName"];
			ringsdk = new SDK(RingCentralAPI_Key, RingCentralAPI_Secret, RingCentralAPI_URL, RingCentralAPI_AppName);

			//Request Refresh of AccessToken or re authenticate user
			if (!ringsdk.Platform.LoggedIn())
			{
				if (Session["RingAuthToken"] != null)
				{
					//Set Auth Data into SDK and refresh Access Token
					ringsdk.Platform.Auth.SetData((Dictionary<string, string>)Session["RingAuthToken"]);
					ringsdk.Platform.Refresh();
				}
				else
				{
					//We have no auth data so Request login (redirectects user to ring central login page)
					//after login - user will be redirected back to here with QueryState "AuthRequest"
					RequestAuthCodeFromRingCentral();
				}
			}
			else
			{
				//ringsdk.Platform.Refresh(); //done by Loggedin Check
			}
		}

		protected void Page_Load(object sender, EventArgs e)
		{
			if (!IsPostBack)
			{
				//Set default time frame
				txtFrom.Text = DateTime.Now.AddMonths(-3).ToString("yyyy-MM-dd");
				txtTo.Text = DateTime.Now.AddDays(1).ToString("yyyy-MM-dd");

				GetCallList();
			}
		}


		protected void btnDeleteMessage_Command(object sender, CommandEventArgs e)
		{
			if (e.CommandName == "DELETE")
			{
				//Not currently in use
				//string messageID = e.CommandArgument.ToString();
				//DeleteMessage(messageID);
			}
		}


		protected void gvCalls_RowCreated(object sender, GridViewRowEventArgs e)
		{

		}

		protected void gvCalls_DataBound(object sender, EventArgs e)
		{

		}

		/// <summary>
		/// Trigger Download of message
		/// I want to look into playing the message through a control on the page.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		protected void btnPlay_Command(object sender, CommandEventArgs e)
		{
			if (e.CommandName == "PLAYMESSAGE")
			{
				string url = e.CommandArgument.ToString();


				var request1 = new Request(url.Substring(url.IndexOf("restapi")));
				var response1 = ringsdk.Platform.Get(request1);
				if (response1.OK)
				{

					Task<byte[]> t = Task.Run(delegate
					{
						return response1.Response.Content.ReadAsByteArrayAsync();
					});

					t.Wait();

					Response.Clear();
					string fileName = "CallManagerDownload.wav";
					Response.AddHeader("content-disposition", "attachment; filename=" + fileName);


					string yourFileMimeType = "audio/x-wav";
					Response.ContentType = yourFileMimeType;


					Response.BinaryWrite(t.Result);
					Response.Flush();
					Response.End();

				}
				else
				{
					txtResponse.Text = "Error..";

				}


			}
		}
		protected void btnDelete_Command(object sender, CommandEventArgs e)
		{
			if (e.CommandName == "DELETEMESSAGE")
			{
				string messageID = e.CommandArgument.ToString();
				DeleteMessage(messageID);
			}
		}

		#endregion



		#region Helpers

		/// <summary>
		/// Redirect to Ring Central to Authenticate User
		/// Ring Central will return user to url of our choosing
		/// </summary>
		private void RequestAuthCodeFromRingCentral()
		{
			string RingCentralAPI_AuthCallBack = ConfigurationManager.AppSettings["RingCentralAPI_AuthCallBack"];

			string WebRequest_URL = ringsdk.Platform.AuthorizeUri(RingCentralAPI_AuthCallBack, "AuthRequest");

			Response.Redirect(WebRequest_URL);
		}

		/// <summary>
		/// Grab all call history for user and their groups and add them to the grid view
		/// </summary>
		private void GetCallList()
		{
			string extensionID = ringsdk.Platform.Auth.GetData()["owner_id"];

			//get time frame extensionIDstrings ready
			DateTime FromDate = DateTime.ParseExact("2017-01-01", "yyyy-MM-dd", null);
			string FromDatestr = FromDate.ToString("yyyy-MM-dd");

			if (DateTime.TryParseExact(txtFrom.Text, "yyyy-MM-dd", null, System.Globalization.DateTimeStyles.None, out FromDate))
			{
				FromDatestr = txtFrom.Text;// DateTime.ParseExact(txtFrom.Text, "dd-MM-yyyy", null).ToString("yyyy-MM-dd");
			}

			DateTime ToDate = DateTime.Now.AddDays(1);
			string ToDatestr = ToDate.ToString("yyyy-MM-dd");

			if (DateTime.TryParseExact(txtTo.Text, "yyyy-MM-dd", null, System.Globalization.DateTimeStyles.None, out ToDate))
			{
				ToDatestr = txtTo.Text;// DateTime.ParseExact(txtTo.Text, "dd-MM-yyyy", null).ToString("yyyy-MM-dd");
			}


			//get a list of my groups
			List<data_Groups> SelectedGroups = new List<data_Groups>();

			using (var cmEntities = new DataLayer.CMEntities())
			{
				var groups = from ug in cmEntities.link_UserGroups where ug.data_Users.RingCentralExtensionID == extensionID select ug.data_Groups;
				SelectedGroups.AddRange(groups);

			}
			List<CallManagementItem> DisplayCalls = new List<CallManagementItem>();
			txtResponse.Text = String.Empty;

			//Get This Users Call Log and add to list

			//var request1 = new Request(string.Format("/restapi/v1.0/account/{0}/extension/{1}/call-log?view=Detailed&withRecording=false&showBlocked=true&dateFrom={2}&dateTo={3}", ConfigurationManager.AppSettings["RingCentralAPI_AccountNumber"], extensionID, FromDatestr, ToDatestr));
			var request1 = new Request(string.Format("/restapi/v1.0/account/{0}/call-log?view=Detailed&showBlocked=true&dateFrom={2}&dateTo={3}", ConfigurationManager.AppSettings["RingCentralAPI_AccountNumber"], extensionID, FromDatestr, ToDatestr));

			var response1 = ringsdk.Platform.Get(request1);
			if (response1.OK)
			{

				foreach (var record in response1.Json["records"].Children())
				{
					string callLegString = String.Empty;
					foreach (var leg in record["legs"])
					{
						callLegString = String.Concat(callLegString, leg["to"]["name"] ?? leg["to"]["phoneNumber"] ?? "[Error]", ", ");
					}
					callLegString = callLegString.Substring(0, callLegString.Length - 2);


					string VoiceMailUri = String.Empty;
					string VoiceMailID = String.Empty;
					string VoiceMailDownloadUri = String.Empty;
					Boolean ShowVoiceMailButtons = false;
					Boolean AddRecord = false;

					
					foreach (var leg in record["legs"])
					{
						if (leg["message"] != null && leg["message"]["uri"] != null)
						{
							VoiceMailUri = leg["message"]["uri"].ToString();
							VoiceMailID = leg["message"]["id"].ToString();
							VoiceMailDownloadUri = String.Format(ConfigurationManager.AppSettings["RingCentralAPI_DownloadLink"], ConfigurationManager.AppSettings["RingCentralAPI_AccountNumber"], extensionID, VoiceMailID);
							ShowVoiceMailButtons = true;
						}
						if (leg["extension"] != null && leg["extension"]["id"] != null)
						{
							if (extensionID == leg["extension"]["id"].ToString())
							{
								AddRecord = true;
							}
							else if (SelectedGroups.Exists(x => x.RingCentralGroupID == leg["extension"]["id"].ToString()))
							{
								AddRecord = true;
							}
						}
					}

					if (AddRecord)
					{
						DisplayCalls.Add(new CallManagementItem
						{
							ID = record["id"].ToString(),
							URI = record["uri"].ToString(),

							VoiceMailUri = VoiceMailUri,
							VoiceMailMessageID = VoiceMailID,
							VoiceMailMessageURI = "",
							VoiceMailDownloadUri = VoiceMailDownloadUri,
							ShowVoiceMailButtons = ShowVoiceMailButtons,
							//VoiceMailMessageURI - used for html audio tag - doesn't seem to be supported with voice mails yet - only call recordings

							FromName = record["from"]["name"] != null ? record["from"]["name"].ToString() : String.Empty,
							ToName = record["to"]["name"] != null ? record["to"]["name"].ToString() : String.Empty,
							DateAndTime = (DateTime)record["startTime"],//DateTime.ParseExact(record["startTime"].ToString(), "yyyy-MM-ddTHH:mm:ss.fffZ", null),//		record["startTime"].ToString()	"11/04/2017 15:35:37"	string


							FromNumber = record["from"]["phoneNumber"] != null ? record["from"]["phoneNumber"].ToString() : record["from"]["extensionNumber"].ToString(),
							ToNumber = record["to"]["phoneNumber"] != null ? record["to"]["phoneNumber"].ToString() : record["to"]["extensionNumber"].ToString(),

							Direction = record["direction"].ToString(),
							Result = record["result"].ToString(),
							Duration = int.Parse(record["duration"].ToString()),

							Legs = record["legs"].Count(),
							LegString = callLegString,

							Source = "Current User"

						});
					}
				}
			}
			else
			{
				txtResponse.Text += "ERROR: " + response1.Body.ToString() + "<br />NEXT<BR /><BR />";
			}


			////loop though each groups call log and add to list
			//foreach (var groupExtension in SelectedGroups)
			//{
			//	var request2 = new Request(string.Format("/restapi/v1.0/account/{0}/extension/{1}/call-log?view=Detailed&withRecording=false&showBlocked=true&dateFrom={2}&dateTo={3}", ConfigurationManager.AppSettings["RingCentralAPI_AccountNumber"], groupExtension.RingCentralGroupID, FromDatestr, ToDatestr));

			//	var response2 = ringsdk.Platform.Get(request2);
			//	if (response2.OK)
			//	{

			//		foreach (var record in response2.Json["records"].Children())
			//		{
			//			string callLegString = String.Empty;
			//			foreach (var leg in record["legs"])
			//			{
			//				callLegString = String.Concat(callLegString, leg["to"]["name"] ?? leg["to"]["phoneNumber"] ?? "[Error]", ", ");
			//			}
			//			callLegString = callLegString.TrimEnd(", ".ToCharArray());


			//			string VoiceMailUri = String.Empty;
			//			string VoiceMailID = String.Empty;
			//			foreach (var leg in record["legs"])
			//			{
			//				if (leg["message"] != null && leg["message"]["uri"] != null)
			//				{
			//					VoiceMailUri = leg["message"]["uri"].ToString();
			//					VoiceMailID = leg["message"]["id"].ToString();
			//				}
			//			}

			//			//if(record[""])
			//			DisplayCalls.Add(new CallManagementItem
			//			{
			//				ID = record["id"].ToString(),
			//				URI = record["uri"].ToString(),
			//				VoiceMailUri = VoiceMailUri,
			//				VoiceMailMessageID = VoiceMailID,
			//				VoiceMailMessageURI = "",
			//				//VoiceMailMessageURI - used for html audio tag - doesn't seem to be supported with voice mails yet - only call recordings

			//				FromName = record["from"]["name"] != null ? record["from"]["name"].ToString() : String.Empty,
			//				ToName = record["to"]["name"] != null ? record["to"]["name"].ToString() : String.Empty,
			//				DateAndTime = (DateTime)record["startTime"],//DateTime.ParseExact(record["startTime"].ToString(), "yyyy-MM-ddTHH:mm:ss.fffZ", null),

			//				FromNumber = record["from"]["phoneNumber"] != null ? record["from"]["phoneNumber"].ToString() : record["from"]["extensionNumber"].ToString(),
			//				ToNumber = record["to"]["phoneNumber"] != null ? record["to"]["phoneNumber"].ToString() : record["to"]["extensionNumber"].ToString(),

			//				Direction = record["direction"].ToString(),
			//				Result = record["result"].ToString(),
			//				Duration = int.Parse(record["duration"].ToString()),

			//				Legs = record["legs"].Count(),
			//				LegString = callLegString,
			//				Source = groupExtension.Name
			//			});
			//		}

			//		//txtResponse.Text += response2.Body.ToString() + "<br />NEXT<BR /><BR />";
			//	}
			//	else
			//	{
			//		txtResponse.Text += "ERROR: " + response2.Body.ToString() + "<br />NEXT<BR /><BR />";
			//	}

			//}

			gvCalls.DataSource = DisplayCalls;
			gvCalls.DataBind();

		}

		private void DeleteMessage(string messageID)
		{

			//DELETE /restapi/v1.0/account/1346632010/extension/1346632010/message-store/315433012010 HTTP/1.1
			string extensionID = ringsdk.Platform.Auth.GetData()["owner_id"];
			List<MessageItem> DisplayMessages = new List<MessageItem>();


			var request1 = new Request(String.Format("restapi/v1.0/account/~/extension/{0}/message-store/{1}", extensionID, messageID));
			var response1 = ringsdk.Platform.Delete(request1);
			if (response1.OK)
			{
				txtResponse.Text = response1.Body.ToString() + "<BR />YOU WILL NEED TO MANUALLY REFRESH NOW.";//temp
			}
			else
			{
				txtResponse.Text = "ERROR: " + response1.Body.ToString();
			}

		}


		#endregion

		public class CallManagementItem
		{
			public string ID { get; set; }
			public string URI { get; set; }
			public string Result { get; set; }
			public string Direction { get; set; }
			public string FromNumber { get; set; }
			public string ToNumber { get; set; }
			public string FromName { get; set; }
			public string ToName { get; set; }
			public DateTime DateAndTime { get; set; }
			public int Legs { get; set; }
			public string LegString { get; set; }
			public int Duration { get; set; }
			public string Source { get; set; }
			public string VoiceMailUri { get; set; }
			public string VoiceMailDownloadUri { get; set; }//used with security token for internal download
			public string VoiceMailMessageID { get; set; }
			public string VoiceMailMessageURI { get; set; }//used for html audio tag
			public Boolean ShowVoiceMailButtons { get; set; }

		}

		protected void btnRefresh_Click(object sender, EventArgs e)
		{
			GetCallList();
		}
	}


}

