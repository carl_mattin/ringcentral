﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="MakeCall.aspx.cs" Inherits="CallManager.Prototype.MakeCall" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
	<asp:Label ID="lblTitle" runat="server" Text="Make a call"></asp:Label>
	
	<br />
	<br />
	<asp:Label ID="lblCallFrom" runat="server" Text="Call From:"></asp:Label>
	<br />
	<asp:DropDownList ID="ddlCallFrom" runat="server"></asp:DropDownList>
	<br />
	<asp:Label ID="lblCallTo" runat="server" Text="Call To:"></asp:Label>
	<br />
	<asp:TextBox ID="txtCallTo" runat="server" Text="+441183347068"></asp:TextBox>
	<br />
	<asp:Button ID="btnMakeCall" runat="server" Text="Call" OnClick="btnMakeCall_Click" />
	<br />
	<asp:Label ID="lblResults" runat="server" Text="use number format = +12053320032"></asp:Label>
</asp:Content>
