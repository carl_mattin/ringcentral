﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="ManageGroups.aspx.cs" Inherits="CallManager.Prototype.ManageGroups" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">

	<asp:Label ID="lblTitle" runat="server" Text="Manage My Groups"></asp:Label>
	
	<asp:GridView ID="gvGroups" runat="server" AutoGenerateColumns="false" OnRowDataBound="gvGroups_RowDataBound">
		<Columns>
			<asp:TemplateField HeaderText="Selected">
				<ItemTemplate>
					<asp:CheckBox ID="chkIncludeGroup" runat="server" />
					<asp:HiddenField ID="hdnID" runat="server" />
				</ItemTemplate>
			</asp:TemplateField>
			<asp:BoundField HeaderText="Group" ReadOnly="true" DataField="GroupName"></asp:BoundField>
			<asp:BoundField HeaderText="Extension" ReadOnly="true" DataField="GroupExtension"></asp:BoundField>
		</Columns>
	</asp:GridView>

	<asp:Button ID="btnSave" runat="server" Text="Save" OnClick="btnSave_Click" />
	<asp:Label ID="lblResults" runat="server"></asp:Label>

</asp:Content>
