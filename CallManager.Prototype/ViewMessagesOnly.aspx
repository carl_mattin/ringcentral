﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="ViewMessagesOnly.aspx.cs" Inherits="CallManager.Prototype.ViewMessagesOnly" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">

		Messages Only
	<asp:GridView ID="gvMessageLog" runat="server" AutoGenerateColumns="false">
		<Columns>
			<asp:BoundField HeaderText="ID" DataField="ID" />
			<asp:BoundField HeaderText="FromNumber" DataField="FromNumber" />
			<asp:BoundField HeaderText="ToNumber" DataField="ToNumber" />
			<asp:BoundField HeaderText="Type" DataField="Type" />
			<asp:BoundField HeaderText="ReadStatus" DataField="ReadStatus" />
			<asp:BoundField HeaderText="Direction" DataField="Direction" />
			<asp:BoundField HeaderText="uri" DataField="uri" />
			<asp:TemplateField HeaderText="DELETE2" >
				<ItemTemplate>
					<asp:Button ID="btnDelete" runat="server" Text="DELETE" CommandName="DELETE" CommandArgument='<%# Eval("ID") %>' OnCommand="btnDelete_Command" />
				</ItemTemplate>
			</asp:TemplateField>
		</Columns>

	</asp:GridView>
	<asp:Label ID="txtResponse" runat="server"></asp:Label>
</asp:Content>
