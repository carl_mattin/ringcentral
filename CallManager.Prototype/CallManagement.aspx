﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="CallManagement.aspx.cs" Inherits="CallManager.Prototype.CallManagement" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
	From:
	<asp:TextBox ID="txtFrom" runat="server" TextMode="Date"></asp:TextBox>
	To:
	<asp:TextBox ID="txtTo" runat="server" TextMode="Date"></asp:TextBox>
	<asp:Button ID="btnRefresh" runat="server" Text="Refresh" OnClick="btnRefresh_Click" />
	<br />
	<asp:GridView ID="gvCalls" runat="server" AutoGenerateColumns="false" OnRowCreated="gvCalls_RowCreated" OnDataBound="gvCalls_DataBound" CssClass="CallGrid" >
		<Columns>
			<asp:BoundField HeaderText="Source" DataField="Source" />
			<asp:BoundField HeaderText="Result" DataField="Result" />
			<asp:BoundField HeaderText="Direction" DataField="Direction" />
			<asp:BoundField HeaderText="FromNumber" DataField="FromNumber" />
			<asp:BoundField HeaderText="ToNumber" DataField="ToNumber" />
			<asp:BoundField HeaderText="FromName" DataField="FromName" />
			<asp:BoundField HeaderText="ToName" DataField="ToName" />
			<asp:BoundField HeaderText="DateAndTime" DataField="DateAndTime" />

			<%--<asp:TemplateField HeaderText="Message">
				<ItemTemplate>
					<asp:Button ID="btnPlayMessage" runat="server" Text="Play" />
					<asp:Button ID="btnDeleteMessage" runat="server" Text="Delete" OnCommand="btnDeleteMessage_Command" CommandName="DELETE" CommandArgument='<%# Eval("VoiceMailID") %>' />
				</ItemTemplate>
			</asp:TemplateField>--%>
			
			<asp:BoundField HeaderText="Legs" DataField="Legs" />
			<asp:BoundField HeaderText="LegString" DataField="LegString" />
			
			<asp:BoundField HeaderText="Duration" DataField="Duration" />	
			<%--<asp:BoundField HeaderText="VoiceMailUri" DataField="VoiceMailUri" />--%>	
			
			<asp:TemplateField HeaderText="Message Actions">
				<ItemTemplate>
					<asp:Button ID="btnDelete" runat="server" Text="DELETE" CommandName="DELETEMESSAGE" CommandArgument='<%# Eval("VoiceMailMessageID") %>' OnCommand="btnDelete_Command" Visible='<%# Eval("ShowVoiceMailButtons") %>' />
					<asp:Literal ID="litSpacer" runat="server" Text=" -- "  Visible='<%# Eval("ShowVoiceMailButtons") %>' ></asp:Literal>
					<asp:Button ID="btnPlay" runat="server" Text="PLAY" CommandName="PLAYMESSAGE" CommandArgument='<%# Eval("VoiceMailDownloadUri") %>' OnCommand="btnPlay_Command" Visible='<%# Eval("ShowVoiceMailButtons") %>' />
		<%--			<audio id="audioPlayer" runat="server"  >
					</audio>--%>
				</ItemTemplate>
			</asp:TemplateField>

		</Columns>
	</asp:GridView>
	<asp:Label ID="txtResponse" runat="server"></asp:Label>
</asp:Content>
