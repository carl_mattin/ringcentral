﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="MessageManagement.aspx.cs" Inherits="CallManager.Prototype.MessageManagement" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
	From:
	<asp:TextBox ID="txtFrom" runat="server" TextMode="Date"></asp:TextBox>
	To:
	<asp:TextBox ID="txtTo" runat="server" TextMode="Date"></asp:TextBox>
	<asp:Button ID="btnRefresh" runat="server" Text="Refresh" OnClick="btnRefresh_Click" />
	<br />
	<asp:GridView ID="gvMessageLog" runat="server" AutoGenerateColumns="false" CssClass="MessageGrid" OnRowDataBound="gvMessageLog_RowDataBound">
		<Columns>
			<asp:BoundField HeaderText="Source" DataField="Source" />
			<asp:BoundField HeaderText="FromName" DataField="FromName" />
			<asp:BoundField HeaderText="FromNumber" DataField="FromNumber" />
			<asp:BoundField HeaderText="ToName" DataField="ToName" />
			<asp:BoundField HeaderText="ToNumber" DataField="ToNumber" />
			<asp:BoundField HeaderText="Type" DataField="Type" />
			<asp:BoundField HeaderText="ReadStatus" DataField="ReadStatus" />
			<asp:BoundField HeaderText="Direction" DataField="Direction" />
			<asp:BoundField HeaderText="AttachmentDuration" DataField="AttachmentDuration" />
			<asp:BoundField HeaderText="DateAndTime" DataField="DateAndTime" />
			<asp:TemplateField HeaderText="Message Actions">
				<ItemTemplate>
					<asp:Button ID="btnDelete" runat="server" Text="DELETE" CommandName="DELETEMESSAGE" CommandArgument='<%# Eval("AttachmentID") %>' OnCommand="btnDelete_Command" />--
					<asp:Button ID="btnPlay" runat="server" Text="PLAY" CommandName="PLAYMESSAGE" CommandArgument='<%# Eval("AttachmentURI") %>' OnCommand="btnPlay_Command" />
		<%--			<audio id="audioPlayer" runat="server"  >
					</audio>--%>
				</ItemTemplate>
			</asp:TemplateField>
		</Columns>

	</asp:GridView>
	<asp:Label ID="txtResponse" runat="server"></asp:Label>
</asp:Content>

